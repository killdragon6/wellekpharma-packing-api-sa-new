package handler

import (
	"main/model"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
)

// Pending Checklis
func Getsainvoice_checklistall(c echo.Context) error {
	res := model.Getsainvoice_checklistall(c)
	return c.JSON(http.StatusOK, res)
}

func Getsainvoice_confirmall(c echo.Context) error {
	res := model.Getsainvoice_confirmall(c)
	return c.JSON(http.StatusOK, res)
}
func Getsa_invoice_number_checklist(c echo.Context) error {
	res := model.Getsa_invoice_number_checklist(c)
	return c.JSON(http.StatusOK, res)
}

func Getsainvoice_checklistbyid(c echo.Context) error {
	res := model.Getsainvoice_checklistbyid(c)
	return c.JSON(http.StatusOK, res)
}

func Addsainvoice_checklist(c echo.Context) error {
	res := model.Addsainvoice_checklist(c)
	return c.JSON(http.StatusOK, res)
}

func Update_sainvoice_checklist(c echo.Context) error {
	res := model.Update_sainvoice_checklist(c)
	return c.JSON(http.StatusOK, res)
}
func Update_sainvoice_checklistbymg(c echo.Context) error {
	res := model.Update_sainvoice_checklistbymg(c)
	return c.JSON(http.StatusOK, res)
}
func Getsa_invoice_number_checklistbyid(c echo.Context) error {
	res := model.Getsa_invoice_number_checklistbyid(c)
	return c.JSON(http.StatusOK, res)
}
func Getsa_work_preparation(c echo.Context) error {
	res := model.Getsa_work_preparation(c)
	return c.JSON(http.StatusOK, res)
}
func Get_customer_by_line(c echo.Context) error {
	res := model.Get_customer_by_line(c)
	return c.JSON(http.StatusOK, res)
}
