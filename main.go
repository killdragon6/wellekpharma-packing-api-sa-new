package main

import (
	"main/handler"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {

	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
	}))
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	// sa
	e.GET("/getsainvoice_checklistall", handler.Getsainvoice_checklistall)
	e.POST("/getsainvoice_checklistbyid", handler.Getsainvoice_checklistbyid)
	e.POST("/addsainvoice_checklist", handler.Addsainvoice_checklist)
	e.POST("/update_sainvoice_checklist", handler.Update_sainvoice_checklist)
	e.POST("/update_sainvoice_checklistbymg", handler.Update_sainvoice_checklistbymg)

	// Port run
	e.GET("/getsainvoice_confirmall", handler.Getsainvoice_confirmall)
	e.GET("/getsa_invoice_number_checklist", handler.Getsa_invoice_number_checklist)
	e.POST("/getsa_invoice_number_checklistbyid", handler.Getsa_invoice_number_checklistbyid)
	e.POST("/getsa_work_preparation", handler.Getsa_work_preparation)
	e.POST("/get_customer_by_line", handler.Get_customer_by_line)

	e.Logger.Fatal(e.Start(":7005"))
}
