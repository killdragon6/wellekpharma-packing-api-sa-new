package model

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"unicode/utf8"

	"github.com/labstack/echo"
)

var DB *sql.DB

func Initialize() {
	db, err := sql.Open("mysql", "root:@/wellekpharma_packing_system")
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Printf("connectDB")
	}
	DB = db
}

// SA
func Getsainvoice_checklistall(c echo.Context) interface{} {
	Initialize()
	var array []interface{}
	query := "SELECT * FROM sa_invoice_checklist where id_invoice_list = 0"
	rows, err := DB.Query(query)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var sabillingtime string
		var linesa string
		var sacustomercode string
		var sacustomername string
		var sabillnumber string
		var employee_name string
		var employee_id string
		var localtion string
		var count_boxs string
		var pk_waiting_checklist_id string
		var start_pack string
		var end_pack string
		var qc_name string
		var qc_id string
		var sa_status string
		var sa_note string
		var id_invoice_list string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string

		rows.Scan(
			&id,
			&sabillingtime,
			&linesa,
			&sacustomercode,
			&sacustomername,
			&sabillnumber,
			&employee_name,
			&employee_id,
			&localtion,
			&count_boxs,
			&pk_waiting_checklist_id,
			&start_pack,
			&end_pack,
			&qc_name,
			&qc_id,
			&sa_status,
			&sa_note,
			&id_invoice_list,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["sabillingtime"] = sabillingtime
		elements["linesa"] = linesa
		elements["sacustomercode"] = sacustomercode
		elements["sacustomername"] = sacustomername
		elements["sabillnumber"] = sabillnumber
		elements["employee_name"] = employee_name
		elements["employee_id"] = employee_id
		elements["localtion"] = localtion
		elements["count_boxs"] = count_boxs
		elements["pk_waiting_checklist_id"] = pk_waiting_checklist_id
		elements["start_pack"] = start_pack
		elements["end_pack"] = end_pack
		elements["qc_name"] = qc_name
		elements["qc_id"] = qc_id
		elements["sa_status"] = sa_status
		elements["sa_note"] = sa_note
		elements["id_invoice_list"] = id_invoice_list
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Getsainvoice_checklistbyid(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	// json_map := make(map[string]interface{})

	var array []interface{}
	// query :=
	rows, err := DB.Query("select * from sa_invoice_checklist where id = ?", json_map["id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var sabillingtime string
		var linesa string
		var sacustomercode string
		var sacustomername string
		var sabillnumber string
		var employee_name string
		var employee_id string
		var localtion string
		var count_boxs string
		var pk_waiting_checklist_id string
		var start_pack string
		var end_pack string
		var qc_name string
		var qc_id string
		var sa_status string
		var sa_note string
		var id_invoice_list string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string

		rows.Scan(
			&id,
			&sabillingtime,
			&linesa,
			&sacustomercode,
			&sacustomername,
			&sabillnumber,
			&employee_name,
			&employee_id,
			&localtion,
			&count_boxs,
			&pk_waiting_checklist_id,
			&start_pack,
			&end_pack,
			&qc_name,
			&qc_id,
			&sa_status,
			&sa_note,
			&id_invoice_list,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["sabillingtime"] = sabillingtime
		elements["linesa"] = linesa
		elements["sacustomercode"] = sacustomercode
		elements["sacustomername"] = sacustomername
		elements["sabillnumber"] = sabillnumber
		elements["employee_name"] = employee_name
		elements["employee_id"] = employee_id
		elements["localtion"] = localtion
		elements["count_boxs"] = count_boxs
		elements["pk_waiting_checklist_id"] = pk_waiting_checklist_id
		elements["start_pack"] = start_pack
		elements["end_pack"] = end_pack
		elements["qc_name"] = qc_name
		elements["qc_id"] = qc_id
		elements["sa_status"] = sa_status
		elements["sa_note"] = sa_note
		elements["id_invoice_list"] = id_invoice_list
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}
	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Update_sainvoice_checklist(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()

	var stringupdate string

	if json_map["id_invoice_list"] != nil {
		stringupdate += "id_invoice_list = '" + json_map["id_invoice_list"].(string) + "',"
	}
	if json_map["sa_status"] != nil {
		stringupdate += "sa_status = '" + json_map["sa_status"].(string) + "',"
	}
	if json_map["modify_date"] != nil {
		stringupdate += "modify_date = '" + json_map["modify_date"].(string) + "',"
	}
	if json_map["modify_at"] != nil {
		stringupdate += "modify_at = '" + json_map["modify_at"].(string) + "',"
	}
	stringupdate = trimLastChar(stringupdate)
	sqlStatement := `
	UPDATE sa_invoice_checklist
	SET ` + stringupdate + `
	WHERE id = ?`
	rows, err := DB.Exec(sqlStatement, json_map["id"])
	if err != nil {
		panic(err)
	}
	if err != nil {
		panic(err)
	}
	count, err := rows.RowsAffected()
	if err != nil {
		panic(err)
	}

	res := make(map[string]interface{})
	res["data"] = count
	return res["data"]
}
func Update_sainvoice_checklistbymg(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()

	var stringupdate string

	if json_map["sabillingtime"] != nil {
		stringupdate += "sabillingtime = '" + json_map["sabillingtime"].(string) + "',"
	}
	if json_map["linesa"] != nil {
		stringupdate += "linesa = '" + json_map["linesa"].(string) + "',"
	}
	if json_map["sacustomercode"] != nil {
		stringupdate += "sacustomercode = '" + json_map["sacustomercode"].(string) + "',"
	}
	if json_map["sacustomername"] != nil {
		stringupdate += "sacustomername = '" + json_map["sacustomername"].(string) + "',"
	}
	if json_map["sabillnumber"] != nil {
		stringupdate += "sabillnumber = '" + json_map["sabillnumber"].(string) + "',"
	}
	if json_map["employee_name"] != nil {
		stringupdate += "employee_name = '" + json_map["employee_name"].(string) + "',"
	}
	if json_map["employee_id"] != nil {
		stringupdate += "employee_id = '" + json_map["employee_id"].(string) + "',"
	}
	if json_map["localtion"] != nil {
		stringupdate += "localtion = '" + json_map["localtion"].(string) + "',"
	}
	if json_map["count_boxs"] != nil {
		stringupdate += "count_boxs = '" + json_map["count_boxs"].(string) + "',"
	}
	if json_map["pk_waiting_checklist_id"] != nil {
		stringupdate += "pk_waiting_checklist_id = '" + json_map["pk_waiting_checklist_id"].(string) + "',"
	}
	if json_map["start_pack"] != nil {
		stringupdate += "start_pack = '" + json_map["start_pack"].(string) + "',"
	}
	if json_map["end_pack"] != nil {
		stringupdate += "end_pack = '" + json_map["end_pack"].(string) + "',"
	}
	if json_map["qc_name"] != nil {
		stringupdate += "qc_name = '" + json_map["qc_name"].(string) + "',"
	}
	if json_map["qc_id"] != nil {
		stringupdate += "qc_id = '" + json_map["qc_id"].(string) + "',"
	}
	if json_map["sa_status"] != nil {
		stringupdate += "sa_status = '" + json_map["sa_status"].(string) + "',"
	}
	if json_map["sa_note"] != nil {
		stringupdate += "sa_note = '" + json_map["sa_note"].(string) + "',"
	}
	if json_map["id_invoice_list"] != nil {
		stringupdate += "id_invoice_list = '" + json_map["id_invoice_list"].(string) + "',"
	}
	if json_map["modify_date"] != nil {
		stringupdate += "modify_date = '" + json_map["modify_date"].(string) + "',"
	}
	if json_map["modify_at"] != nil {
		stringupdate += "modify_at = '" + json_map["modify_at"].(string) + "',"
	}
	stringupdate = trimLastChar(stringupdate)
	sqlStatement := `
	UPDATE sa_invoice_checklist
	SET ` + stringupdate + `
	WHERE id = ?`
	rows, err := DB.Exec(sqlStatement, json_map["id"])
	if err != nil {
		panic(err)
	}
	if err != nil {
		panic(err)
	}
	count, err := rows.RowsAffected()
	if err != nil {
		panic(err)
	}

	res := make(map[string]interface{})
	res["data"] = count
	return res["data"]
}
func Addsainvoice_checklist(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	// json_map := make(map[string]interface{})

	rows, err := DB.Exec("INSERT INTO sa_invoice_number_checklist(`invoice_number`, `invoice_price`, `create_date`, `create_at`, `modify_date`, `modify_at`) VALUES (?,?,?,?,?,?)", json_map["invoice_number"], json_map["invoice_price"], json_map["create_date"], json_map["create_at"], json_map["modify_date"], json_map["modify_at"])

	if err != nil {
		panic(err)
	}
	lastID, err := rows.LastInsertId()
	println("The last inserted row id:", lastID)
	if err != nil {
		panic(err)
	}

	res := make(map[string]interface{})

	res["data"] = lastID
	return res["data"]
}
func Getsa_invoice_number_checklist(c echo.Context) interface{} {
	Initialize()
	var array []interface{}
	query := "SELECT * FROM sa_invoice_number_checklist where id_invoice_list != 0"
	rows, err := DB.Query(query)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var invoice_number string
		var invoice_price string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string

		rows.Scan(
			&id,
			&invoice_number,
			&invoice_price,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["invoice_number"] = invoice_number
		elements["invoice_price"] = invoice_price
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Getsa_invoice_number_checklistbyid(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	var array []interface{}
	rows, err := DB.Query("select * from sa_invoice_number_checklist where id = ?", json_map["id_invoice_list"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var invoice_number string
		var invoice_price string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string

		rows.Scan(
			&id,
			&invoice_number,
			&invoice_price,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["invoice_number"] = invoice_number
		elements["invoice_price"] = invoice_price
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Getsainvoice_confirmall(c echo.Context) interface{} {
	Initialize()
	var array []interface{}
	query := "SELECT * FROM sa_invoice_checklist where id_invoice_list != 0 and sa_status = 'waiting'"
	rows, err := DB.Query(query)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var sabillingtime string
		var linesa string
		var sacustomercode string
		var sacustomername string
		var sabillnumber string
		var employee_name string
		var employee_id string
		var localtion string
		var count_boxs string
		var pk_waiting_checklist_id string
		var start_pack string
		var end_pack string
		var qc_name string
		var qc_id string
		var sa_status string
		var sa_note string
		var id_invoice_list string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string

		rows.Scan(
			&id,
			&sabillingtime,
			&linesa,
			&sacustomercode,
			&sacustomername,
			&sabillnumber,
			&employee_name,
			&employee_id,
			&localtion,
			&count_boxs,
			&pk_waiting_checklist_id,
			&start_pack,
			&end_pack,
			&qc_name,
			&qc_id,
			&sa_status,
			&sa_note,
			&id_invoice_list,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["sabillingtime"] = sabillingtime
		elements["linesa"] = linesa
		elements["sacustomercode"] = sacustomercode
		elements["sacustomername"] = sacustomername
		elements["sabillnumber"] = sabillnumber
		elements["employee_name"] = employee_name
		elements["employee_id"] = employee_id
		elements["localtion"] = localtion
		elements["count_boxs"] = count_boxs
		elements["pk_waiting_checklist_id"] = pk_waiting_checklist_id
		elements["start_pack"] = start_pack
		elements["end_pack"] = end_pack
		elements["qc_name"] = qc_name
		elements["qc_id"] = qc_id
		elements["sa_status"] = sa_status
		elements["sa_note"] = sa_note
		elements["id_invoice_list"] = id_invoice_list
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Getsa_work_preparation(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	// json_map := make(map[string]interface{})

	var array []interface{}
	// query :=
	rows, err := DB.Query("SELECT * FROM `sa_invoice_checklist` WHERE `sa_status` = 'sa_ok' AND `linesa` = ? ", json_map["linesa"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var sabillingtime string
		var linesa string
		var sacustomercode string
		var sacustomername string
		var sabillnumber string
		var employee_name string
		var employee_id string
		var localtion string
		var count_boxs string
		var pk_waiting_checklist_id string
		var start_pack string
		var end_pack string
		var qc_name string
		var qc_id string
		var sa_status string
		var sa_note string
		var id_invoice_list string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string

		rows.Scan(
			&id,
			&sabillingtime,
			&linesa,
			&sacustomercode,
			&sacustomername,
			&sabillnumber,
			&employee_name,
			&employee_id,
			&localtion,
			&count_boxs,
			&pk_waiting_checklist_id,
			&start_pack,
			&end_pack,
			&qc_name,
			&qc_id,
			&sa_status,
			&sa_note,
			&id_invoice_list,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["sabillingtime"] = sabillingtime
		elements["linesa"] = linesa
		elements["sacustomercode"] = sacustomercode
		elements["sacustomername"] = sacustomername
		elements["sabillnumber"] = sabillnumber
		elements["employee_name"] = employee_name
		elements["employee_id"] = employee_id
		elements["localtion"] = localtion
		elements["count_boxs"] = count_boxs
		elements["pk_waiting_checklist_id"] = pk_waiting_checklist_id
		elements["start_pack"] = start_pack
		elements["end_pack"] = end_pack
		elements["qc_name"] = qc_name
		elements["qc_id"] = qc_id
		elements["sa_status"] = sa_status
		elements["sa_note"] = sa_note
		elements["id_invoice_list"] = id_invoice_list
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}
	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Get_customer_by_line(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	// json_map := make(map[string]interface{})

	var array []interface{}
	// query :=
	rows, err := DB.Query("SELECT * FROM `sa_invoice_checklist` WHERE `sa_status` = 'sa_ok' AND `linesa` = ? ", json_map["linesa"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var sabillingtime string
		var linesa string
		var sacustomercode string
		var sacustomername string
		var sabillnumber string
		var employee_name string
		var employee_id string
		var localtion string
		var count_boxs string
		var pk_waiting_checklist_id string
		var start_pack string
		var end_pack string
		var qc_name string
		var qc_id string
		var sa_status string
		var sa_note string
		var id_invoice_list string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string

		rows.Scan(
			&id,
			&sabillingtime,
			&linesa,
			&sacustomercode,
			&sacustomername,
			&sabillnumber,
			&employee_name,
			&employee_id,
			&localtion,
			&count_boxs,
			&pk_waiting_checklist_id,
			&start_pack,
			&end_pack,
			&qc_name,
			&qc_id,
			&sa_status,
			&sa_note,
			&id_invoice_list,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["sabillingtime"] = sabillingtime
		elements["linesa"] = linesa
		elements["sacustomercode"] = sacustomercode
		elements["sacustomername"] = sacustomername
		elements["sabillnumber"] = sabillnumber
		elements["employee_name"] = employee_name
		elements["employee_id"] = employee_id
		elements["localtion"] = localtion
		elements["count_boxs"] = count_boxs
		elements["pk_waiting_checklist_id"] = pk_waiting_checklist_id
		elements["start_pack"] = start_pack
		elements["end_pack"] = end_pack
		elements["qc_name"] = qc_name
		elements["qc_id"] = qc_id
		elements["sa_status"] = sa_status
		elements["sa_note"] = sa_note
		elements["id_invoice_list"] = id_invoice_list
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}
	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func trimLastChar(s string) string {
	r, size := utf8.DecodeLastRuneInString(s)
	if r == utf8.RuneError && (size == 0 || size == 1) {
		size = 0
	}
	return s[:len(s)-size]
}
